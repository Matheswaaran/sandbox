import React from 'react';

class InputField extends React.Component{
  constructor(props){
    super(props);
    this.state = {};
  }

  render(){
    const value = this.props.value;
    let element;
    switch (value.tag) {
      case "input":
        element = <input type={value.type} required={value.required} />;
        break;
      // case "select":
      //   element = <select onChange={this.onChangeHandler}>{value.values.map((opt) => <option key={opt.toString()} value={opt}>{opt}</option>)}</select>;
      //   break;
      default:
        element = null;
    }
    return(
      <div>{value.label + ":"}
      {element}
      <br></br>
      <br></br>
      </div>
    )
  }
}

export default InputField;
