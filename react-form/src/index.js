import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const input_elements = [
  {tag: "input", type: "text", id: "first_name", label:"First Name", required: true},
  {tag: "input", type: "text", id: "last_name", label:"Last Name", required: true},
  {tag: "input", type: "text", id: "username", label:"Username", required: true},
  {tag: "input", type: "email", id: "email_id", label:"Email Address", required: true},
  {tag: "input", type: "text", id: "contact_no", label:"Contact No", required: true},
  {tag: "input", type: "password", id: "password", label:"Password", required: true},
  {tag: "input", type: "password", id: "re_password", label:"Retype Password", required: true},
  {tag: "select", id: "gender", label: "Gender", values: ["Male","Female","Others"]}
];
const submitURL = "https://www.asdf.c"

ReactDOM.render(<App elements={input_elements} submitURL={submitURL} />, document.getElementById('root'));
