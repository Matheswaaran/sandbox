import React from 'react';

class FormValidate extends React.Component{
  constructor(props){
    super(props);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.state = {email_warning : true, password_warning: true};
  }

  onChangeHandler(event){
    this.setState({
      [event.target.id]: event.target.value
    });

    if (event.target.id === "email_id") {
      let re = /\S+@\S+\.\S+/;
      let email = event.target.value;
      re.test(email) || email === "" ? this.setState({email_warning: true}) : this.setState({email_warning: false});
    }
    if (event.target.id === "password") {
      let re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30}/;
      let password = event.target.value;
      re.test(password) || password === "" ? this.setState({password_warning: true}) : this.setState({password_warning: false});
    }
  }

  onSubmitHandler(event){
    alert("Form Submitted");
    console.log(this.state);
    event.preventDefault();
  }

  render(){
    const elements = this.props.elements;
    return (
      <form onSubmit={this.onSubmitHandler}>
        {elements.map(item => {
          if(item.tag === "input") {
            return(
              <div key={item.id}> {item.label}:
                <input type={item.type} id={item.id} required={item.required} onKeyUp={this.onChangeHandler} /> <br/><br/>
                {item.id === "email_id" ? (this.state.email_warning ? null : <p>invalid</p>) : (item.id === "password" ? (this.state.password_warning ? null : <p>invalid</p>) : null)}
              </div>
            )
          }else if(item.tag === "select"){
            return(
              <div key={item.id}> {item.label}:
                <select id={item.id} onChange={this.onChangeHandler}>{item.values.map((value) => {
                    return(<option key={value.toString()} value={value}>{value}</option>)
                  })}</select><br/><br/>
              </div>
            )
          }
          return null;
       })}
         <input type="submit"/>
      </form>
    );
  }
}

export default FormValidate;
