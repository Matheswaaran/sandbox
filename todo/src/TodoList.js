import React from 'react';
import ListItem from './ListItem';

class TodoList extends React.Component {
  constructor(props){
    super(props);
    this.onSubmitHandler = this.onSubmitHandler.bind(this);
    this.onInputChangeHandler = this.onInputChangeHandler.bind(this);
    this.state = {currentValue: ""};
  }

  onInputChangeHandler(e){
    this.setState({currentValue : e.target.value});
  }

  onSubmitHandler(e){
    e.preventDefault();
    if (this.state.currentValue !== "") {
      const data = this.state.currentValue;
      this.props.action.add_todo(data,this.props.index);
      this.setState({currentValue: ""});
    }
  }


  render() {
    const actions = {
      change_item: this.props.action.change_item,
      change_checked: this.props.action.change_checked
    };
    return (
      <div>
        <h1>To Do List </h1>
        <form id="main_input_box">
          <label>
          <input type="text" className ="form-control" id="custom_textbox" name="Item" placeholder="What do you need to do?" onChange={this.onInputChangeHandler} value={this.state.currentValue}/>
          <input type="submit" value="Add" className="btn btn-primary add_button" onClick={this.onSubmitHandler} required autoFocus/>
          </label>
        </form>
        {this.props.todos.map((todo,i) => <ListItem key={i.toString()} index={i} catagoryIndex={this.props.index} val={todo} action={actions} />)}
      </div>
    );
  }
}

export default TodoList;
