import React from 'react';

class ListItem extends React.Component{
  constructor(props){
    super(props);
    this.deleteHandler = this.deleteHandler.bind(this);
    this.editHandler = this.editHandler.bind(this);
    this.changeChecked = this.changeChecked.bind(this);
    this.onEditFormSubmitHandler = this.onEditFormSubmitHandler.bind(this);
    this.editValueChangeHandler = this.editValueChangeHandler.bind(this);
    this.state = {edit_click : false, currentValue: ""};
  }

  deleteHandler(e){
    e.preventDefault();
    this.props.action.change_item("delete", this.props.catagoryIndex, this.props.index, this.props.val);
  }

  editHandler(e){
    e.preventDefault();
    this.setState(prevState => ({edit_click: !prevState.edit_click}));
  }

  onEditFormSubmitHandler(e){
    e.preventDefault();
    const currentValue = {list_data: this.state.currentValue, checked: this.props.val.checked };
    this.props.action.change_item("edit", this.props.catagoryIndex, this.props.index, currentValue);
    this.setState({edit_click : false, currentValue: ""});
  }

  editValueChangeHandler(e){
    this.setState({currentValue: e.target.value});
  }

  changeChecked(){
    this.props.action.change_checked(this.props.catagoryIndex, this.props.index);
  }

  render(){
    return(
      <form>
        <ol className="list-group list_of_items">
        <li className={"list-group-item"}>
          {!this.state.edit_click ?
          (<div className={"text_holder" + (this.props.val.checked ? " completed_item" : " ")}>
            <input checked={this.props.val.checked} type="checkbox" onChange={this.changeChecked} /> {this.props.val.list_data}
            <button className="delete btn btn-warning pull-right" onClick={this.deleteHandler}>Delete</button>
            <button className="edit btn btn-success pull-right" onClick={this.editHandler}>Edit</button><br/>
          </div>)
          : (<form className='edit_input_box' onSubmit={this.onEditFormSubmitHandler}><input type='text' value={this.state.currentValue} onChange={this.editValueChangeHandler} className='itembox' autoFocus required/></form>)}
          <div className="checkbox"></div>
        </li>
        </ol>
      </form>
    );
  }
}
export default ListItem;
