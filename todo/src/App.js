import React from 'react';
import './index.css'
import TodoList from './TodoList';

class Catagory extends React.Component {
  constructor(props){
    super(props);
    this.onCatagorySubmitHandler = this.onCatagorySubmitHandler.bind(this);
    this.onCatagoryInputChangeHandler = this.onCatagoryInputChangeHandler.bind(this);
    this.catagoryClickHandler = this.catagoryClickHandler.bind(this);
    this.addToDoHandler = this.addToDoHandler.bind(this);
    this.changeListItem = this.changeListItem.bind(this);
    this.changeItemCheckedHandler = this.changeItemCheckedHandler.bind(this);
    this.onCatagoryDeleteHandler = this.onCatagoryDeleteHandler.bind(this);
    this.onCatagoryEditHandler = this.onCatagoryEditHandler.bind(this);
    this.editCatagoryValueChangeHandler = this.editCatagoryValueChangeHandler.bind(this);
    this.onCatagoryEditFormSubmitHandler = this.onCatagoryEditFormSubmitHandler.bind(this);
    this.state={data:[], catagory_input: "", catagory_selected: 0, catagaory_edit_input: ""};
  }

  onCatagoryInputChangeHandler(e){
    this.setState({catagory_input: e.target.value});
  }

  onCatagorySubmitHandler(e){
    e.preventDefault();
    if (this.state.catagory_input !== "") {
      const array = this.state.data;
      array.push({catagory: this.state.catagory_input, todos: [], edit_selected:false});
      this.setState({data: array, catagory_input:""});
    }
  }

  addToDoHandler(data,index){
    const result = this.state.data;
    result[index].todos.push({list_data: data, checked: false})
    this.setState({data: result});
  }

  changeItemCheckedHandler(catagoryIndex, index){
    const result = this.state.data;
    result[catagoryIndex].todos[index].checked = !result[catagoryIndex].todos[index].checked;
    this.setState({data: result});
  }

  catagoryClickHandler(selected){
    this.setState({catagory_selected: selected});
  }

  changeListItem(action, catagoryIndex, index, data){
    const result = this.state.data;
    if(action === "edit"){
      result[catagoryIndex].todos.splice(index, 1, data);
    }else if(action === "delete"){
      result[catagoryIndex].todos.splice(index, 1);
    }
    this.setState({data: result});
  }

  onCatagoryDeleteHandler(i,e){
    e.preventDefault();
    const result = this.state.data;
    result.splice(i, 1);
    this.setState({data: result, catagory_selected: 0});
  }

  onCatagoryEditHandler(i, e){
    e.preventDefault();
    const result = this.state.data;
    result[i].edit_selected = true;
    this.setState({data: result});
  }

  editCatagoryValueChangeHandler(e){
    this.setState({catagaory_edit_input: e.target.value});
  }

  onCatagoryEditFormSubmitHandler(i){
    if (this.state.catagaory_edit_input !== "") {
      const result = [...this.state.data];
      const todos_data = this.state.data[i].todos;
      result.splice(i, 1, {catagory: this.state.catagaory_edit_input, todos: todos_data, edit_selected: false});
      this.setState({data: result, catagaory_edit_input: ""});
    }
  }

  render() {
    const actions = {
      add_todo: this.addToDoHandler,
      change_item: this.changeListItem,
      change_checked: this.changeItemCheckedHandler
    };
    return (
      <div>
        <div className="col-lg-2"></div>
        <div className="container col-lg-4" id="main">
          <h1>Catagories</h1>
          {this.state.data.map((object, i)=>
            { return object.edit_selected === false ?
            (<div key={i.toString()} onClick={this.catagoryClickHandler.bind(this, i)}>
              <h3 className={i===this.state.catagory_selected? "catagory_selected" : null}>{object.catagory}</h3>
                <button className="delete btn btn-warning pull-right" onClick={this.onCatagoryDeleteHandler.bind(this, i)}>Delete</button>
                <button className="edit btn btn-success pull-right" onClick={this.onCatagoryEditHandler.bind(this, i)}>Edit</button><br/>
              <hr/>
            </div>) : (<form key={i.toString()} className='edit_input_box' onSubmit={this.onCatagoryEditFormSubmitHandler.bind(this, i)}><input type='text' value={this.state.catagaory_edit_input} onChange={this.editCatagoryValueChangeHandler} className='itembox' autoFocus required/></form>)
          }
          )}
          <form id="main_input_box">
            <input type="text" className ="form-control" id="custom_textbox" name="Item" placeholder="Add a catagory" value={this.state.catagory_input} onChange={this.onCatagoryInputChangeHandler} autoFocus required/>
            <input type="submit" value="Add" className="btn btn-primary add_button" onClick={this.onCatagorySubmitHandler}/>
          </form>
        </div>
        <div className="container col-lg-4" id="main">
          {(typeof this.state.data[this.state.catagory_selected] !== "undefined") ?
            <TodoList todos={this.state.data[this.state.catagory_selected].todos} index={this.state.catagory_selected} action={actions}/>
            : <h2>Add / Select a new catagory first</h2>}
        </div>
      </div>
    );
  }
}

export default Catagory;
