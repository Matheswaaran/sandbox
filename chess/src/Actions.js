function setKnightPosition(kx, ky) {
  return {
    type: 'MOVE_KNIGHT',
    position: {kx, ky}
  };
}

export default setKnightPosition;
