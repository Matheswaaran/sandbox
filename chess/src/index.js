import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import ChessReducer from './Reducers';

const store = createStore(
  ChessReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


ReactDOM.render(
  <Provider store={store}>
    <div style={{ width: '500px', height:'500px', border:'1px', textAlign: 'center'}}>
      <App />
    </div>
  </Provider>, document.getElementById('root'));
