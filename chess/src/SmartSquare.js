import React from 'react';
import ChessSquare from './ChessSquare';
import { DropTarget } from 'react-dnd';

const SquareTarget = {
  canDrop(props){
    const { canMoveKnight, position: {x,y}} = props;
    return canMoveKnight(x,y);
  },
  drop(props){
    const { moveKnight, position: {x, y}} = props;
    moveKnight(x, y);
  }
}

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  };
}

class SmartSquare extends React.Component{

  renderOverlay(color) {
    return (
      <div style={{
        position: 'absolute',
        top: 0,
        left: 0,
        height: '100%',
        width: '100%',
        zIndex: 1,
        opacity: 0.5,
        backgroundColor: color,
      }} ></div>
    );
  }


  render() {
      const { position: {x, y}, connectDropTarget, isOver, canDrop } = this.props;
      const black = (x + y) % 2 === 1;

      return connectDropTarget(
        <div style={{ position: 'relative', width: '100%', height: '100%'}}>
          <ChessSquare black={black}>
            {this.props.children}
          </ChessSquare>
          {isOver && !canDrop && this.renderOverlay('red')}
          {!isOver && canDrop && this.renderOverlay('yellow')}
          {isOver && canDrop && this.renderOverlay('green')}
        </div>
      );
    }
  }

export default DropTarget('knight', SquareTarget, collect)(SmartSquare);
