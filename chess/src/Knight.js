import React from 'react';
import { DragSource } from 'react-dnd';

const KnightSource = {
  beginDrag(props){
    return {};
  }
}

function collect(connect, monitor){
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

class Knight extends React.Component{
  render(){
    const { connectDragSource, isDragging } = this.props;
    return connectDragSource(
      <div style={{
        opacity: isDragging ? 0.5 : 1,
        fontSize: 40,
        fontWeight: 'bold',
        cursor: 'move'
        }}>
        ♞
      </div>
    )
  }
}

export default DragSource('knight', KnightSource, collect)(Knight);
