import React from 'react';

class ChessSquare extends React.Component{
  render(){
    return (<div style={{
      backgroundColor: this.props.black ? 'black' : 'white',
      color : this.props.black ? 'white' : 'black',
      width: '100%',
      height: '100%'
    }}>
    {this.props.children}
  </div>);
  }
}

export default ChessSquare;
