import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import SmartSquare from './SmartSquare';
import Knight from './Knight';
import setKnightPosition from './Actions';

class App extends Component {

  canMoveKnight = (toX, toY) => {
    const {kx, ky} = this.props.position;
    const dx = toX - kx;
    const dy = toY - ky;

    return (Math.abs(dx) === 2 && Math.abs(dy) === 1) ||
           (Math.abs(dx) === 1 && Math.abs(dy) === 2);
  }

  moveKnight = (toX, toY) => {
    if(!this.canMoveKnight(toX, toY)) return false;
    this.props.setKnightPosition(toX, toY);
  }

  renderPeice = (x, y) => {
    const {kx, ky} = this.props.position;
    return (x === kx && y === ky) ? <Knight /> : null;
  }

  renderBoard = (i) => {
    const x = i % 8;
    const y = Math.floor(i / 8);

    return (
      <div key={i} style={{ width: '12.5%', height: '12.5%' }}>
        <SmartSquare
          canMoveKnight={this.canMoveKnight}
          moveKnight={this.moveKnight}
          position={{x,y}}>
          {this.renderPeice(x, y)}
        </SmartSquare><br/>
      </div>
    );
  }

  render() {
    const squares = [];
    for (let i = 0; i < 64; i++) {
      squares.push(this.renderBoard(i));
    }

    return (
      <div style={{
        width: '100%',
        height: '100%',
        display: 'flex',
        flexWrap: 'wrap'
      }}>
        {squares}
      </div>
    );
  }
}

const mapStateToProps = (state) => state;
export default connect(mapStateToProps, {setKnightPosition})(DragDropContext(HTML5Backend)(App));
