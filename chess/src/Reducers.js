function ChessReducer (state = { position: {kx: 0, ky: 0}}, action){
  const {position} = action;
  switch (action.type) {
    case 'MOVE_KNIGHT':
      return { ...state, position };
      break;
    default:
      return state;
  }
}

export default ChessReducer;
