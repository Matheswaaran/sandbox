import React, { Component } from 'react';
import Posts from './Posts';

function Blog(props){
  const posts = props.posts;

  const elements = posts.map((post) => <Posts key={post.id} value={post} />);
  return elements;
}
export default Blog;
