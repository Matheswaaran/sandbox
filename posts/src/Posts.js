import React, { Component } from 'react';

function Posts(props){
  return(
    <div>
    <h1>{props.value.title}</h1>
    <p>{props.value.content}</p>
    <hr></hr>
    </div>
  );
}

export default Posts;
