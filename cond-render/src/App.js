import React, { Component } from 'react';
import Greetings from './Greetings';
import { LoginButton, LogoutButton } from './Buttons'

class LoginControl extends React.Component{
  constructor(props){
    super(props);
    this.loginClick = this.loginClick.bind(this);
    this.logoutClick = this.logoutClick.bind(this);
    this.state = {isLoggedin: false}
  }

  loginClick(){
    this.setState({isLoggedin: true});
  }

  logoutClick(){
    this.setState({isLoggedin: false});
  }

  render(){
    const isLoggedin = this.state.isLoggedin;
    let button;

    // if(isLoggedin){
    //   button = <LogoutButton action={this.logoutClick} />;
    // }else{
    //   button = <LoginButton action={this.loginClick} />;
    // }

    return(
      <div>
        <Greetings isLoggedin={isLoggedin} />
        {isLoggedin ? <LogoutButton action={this.logoutClick} /> : <LoginButton action={this.loginClick} /> }
        </div>
    )
  }
}


export default LoginControl;
