import React, { Component } from 'react';

function UserGreeting(props){
  return(<h1>Welcome User!</h1>)
}

function GuestGreeting(props){
  return(<h1>Please SignIn!</h1>)
}

class Greeting extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    const isLoggedin = this.props.isLoggedin;
    if(isLoggedin){
      return <UserGreeting />
    }
    return <GuestGreeting />
  }
}

export default Greeting;
