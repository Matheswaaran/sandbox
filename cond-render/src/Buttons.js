import React, { Component } from 'react';

function LoginButton(props){
  return (<button onClick={props.action}>Login</button>);
}

function LogoutButton(props){
  return (<button onClick={props.action}>Logout</button>);
}


export {LoginButton, LogoutButton};
