import React from 'react';

const scaleName = {
  c: "Celsius",
  f: "Fahrenheit"
}

function convert(temprature, toConvert){
  const input = parseFloat(temprature);
  if(Number.isNaN(input)){
    return "";
  }
  if(toConvert === "toCelsius"){
    return Math.round((temprature - 32) * 5 / 9).toString();
  }else if (toConvert === "toFahrenheit"){
    return Math.round((temprature * 9 / 5) + 32).toString();
  }
}

class InputElement extends React.Component{
  constructor(props){
    super(props);
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  onChangeHandler(e){
    this.props.action(e.target.value);
  }

  render(){
    const scale = this.props.scale;
    const temprature = this.props.temprature;
    return(
      <fieldset>
        <legend>{scaleName[scale]} Value</legend>
        <input  value={temprature} onChange={this.onChangeHandler} />
      </fieldset>
    );
  }
}

class Calc extends React.Component{
  constructor(props){
    super(props);
    this.state = {scale: 'c', temprature: 0};
    this.onCelciusChange = this.onCelciusChange.bind(this);
    this.onFahrenheitChange = this.onFahrenheitChange.bind(this);
  }

  onCelciusChange(temprature){
    this.setState({scale: 'c', temprature});
  }

  onFahrenheitChange(temprature){
    this.setState({scale: 'f', temprature});
  }

  render(){
    const temprature = this.state.temprature;
    const scale = this.state.scale;
    const cel = scale === 'c' ? temprature : convert(temprature,"toCelsius");
    const fah = scale === 'f' ? temprature : convert(temprature,"toFahrenheit");
    return(
      <div>
        <InputElement scale='c' temprature={cel} action={this.onCelciusChange}/>
        <InputElement scale='f' temprature={fah} action={this.onFahrenheitChange}/>
      </div>);
  }
}

export default Calc;
