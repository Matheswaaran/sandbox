import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";

function WarningBanner(props) {
  if (!props.warn) {
    return null;
  }
  return <div className="Warning">Warning!</div>;
}

class Body extends React.Component {
  constructor(props) {
    super(props);
    this.state = { warning: false };
    this.changeWarning = this.changeWarning.bind(this);
  }

  changeWarning() {
    this.setState(prevState => ({
      warning: !prevState.warning
    }));
  }

  render() {
    return (
      <div>
        <WarningBanner warn={this.state.warning} />
        <button onClick={this.changeWarning}>
          {this.state.warning ? "Hide" : "Show"}
        </button>
      </div>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<Body />, rootElement);
